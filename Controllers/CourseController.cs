﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Test.ViewModels;
using Test.Models;

namespace Test.Controllers
{
    public class CourseController : Controller
    {
        // GET: Course
        public ActionResult Index()
        {
            CourseListViewModel courseListViewModel = new CourseListViewModel();

            CourseLayer cBal = new CourseLayer();
            List<Courses> courses = cBal.GetCourse();

            List<CourseViewModel> cViewModels = new List<CourseViewModel>();

            foreach (Courses c in courses)
            {
                CourseViewModel cViewModel = new CourseViewModel();
                cViewModel.CourseNo = c.CourseNo;
                cViewModel.CourseName = c.CourseName;
                cViewModel.Credit = c.Credit;
                cViewModels.Add(cViewModel);
            }
            courseListViewModel.course = cViewModels;
            return View("Index", courseListViewModel);
        }

        public ActionResult AddNew()
        {
            return View("AddCourse");
        }
        public ActionResult SaveCourse(Courses c, string BtnSubmit)
        {
            switch (BtnSubmit)
            {
                case "Save Course":
                    if (ModelState.IsValid)
                    {
                        CourseLayer cBal = new CourseLayer();
                        cBal.SaveCourse(c);
                        return RedirectToAction("Index");
                    }
                    else
                    {
                        return View("CreateCourse");
                    }

                case "Cancel":
                    return RedirectToAction("Index");
            }
            return new EmptyResult();
        }
    }
}