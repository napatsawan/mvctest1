﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Test.Models;
using Test.ViewModels;

namespace Test.Controllers
{
    public class EnrolmentController : Controller
    {
        // GET: Enrolment
        public ActionResult Index()
        {
            EnrolmentListViewModel enrolmentListViewModel = new EnrolmentListViewModel();

            EnrolmentLayer enlBal = new EnrolmentLayer();
            List<Enrolment> enrolment = enlBal.GetEnrolment();

            List<EnrolmentViewModel> enlViewModels = new List<EnrolmentViewModel>();

            foreach (Enrolment enl in enrolment)
            {
                EnrolmentViewModel enlViewModel = new EnrolmentViewModel();
                enlViewModel.ID = enl.ID;
                enlViewModel.StudentID = enl.StudentID;
                enlViewModel.CourseNo = enl.CourseNo;
                enlViewModel.Grade = enl.Grade;
                enlViewModels.Add(enlViewModel);
            }
            enrolmentListViewModel.enrolment = enlViewModels;
            return View("Index", enrolmentListViewModel);
        }

        public ActionResult AddNew()
        {
            return View("AddEnrolment");
        }
        public ActionResult SaveEnrolment(Enrolment enl, string BtnSubmit)
        {
            switch (BtnSubmit)
            {
                case "Save Enrolment":
                    if (ModelState.IsValid)
                    {
                        EnrolmentLayer enlBal = new EnrolmentLayer();
                        enlBal.SaveEnrolment(enl);
                        return RedirectToAction("Index");
                    }
                    else
                    {
                        return View("AddEnrolment");
                    }

                case "Cancel":
                    return RedirectToAction("Index");
            }
            return new EmptyResult();
        }
    }
}