﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
namespace Test.Models
{
    public class Courses
    {
        [Key]
        public string CourseNo { get; set; }

        public string CourseName { get; set; }

        public int Credit { get; set; }
    }
  
}