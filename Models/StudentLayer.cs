﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Test.Models;
using Test.ViewModels;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;

namespace Test.Models
{
    public class StudentLayer
    {
        public List<Student> GetStudent()
        {
            DataAccessLayer DALStd = new DataAccessLayer();
            return DALStd.student.ToList();
        }
        public Student SaveStudent(Student s)
        {
            DataAccessLayer std_Dal = new DataAccessLayer();
            std_Dal.student.Add(s);
            std_Dal.SaveChanges();
            return s;
        }
        public bool CheckStudentID(string stdID)
        {
            List<Student> students = this.GetStudent();
            foreach (Student item in students)
            {
                if (item.StudentID == stdID)
                {
                    return true;
                }
            }
            return false;
        }

        public void UpdateGPA(double gpa, string stdID)
        {
            DataAccessLayer DALStd = new DataAccessLayer();
            Student stdRemove = DALStd.student.Find(stdID);
            Student stdUpdate = new Student();
            stdUpdate.StudentID = stdRemove.StudentID;
            stdUpdate.FirstName = stdRemove.FirstName;
            stdUpdate.Lastname = stdRemove.Lastname;
            stdUpdate.Year = stdRemove.Year;
            stdUpdate.GPA = gpa;
            DALStd.student.Remove(stdRemove);
            DALStd.SaveChanges();
            SaveStudent(stdUpdate);


        }
    }
}