﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Test.Models;
using Test.ViewModels;

namespace Test.Models
{
    public class CourseLayer
    {
        public List<Courses> GetCourse()
        {
            DataAccessLayer DALCourse = new DataAccessLayer();
            return DALCourse.course.ToList();
        }
        public Courses SaveCourse(Courses c)
        {
            DataAccessLayer c_Dal = new DataAccessLayer();
            c_Dal.course.Add(c);
            c_Dal.SaveChanges();
            return c;
        }
        public bool CheckCourseNo(string cNo)
        {
            List<Courses> courses = this.GetCourse();
            foreach (Courses item in courses)
            {
                if (item.CourseNo == cNo)
                {
                    return true;
                }
            }
            return false;
        }
        public int getCredit(string CourseNo)
        {
            List<Courses> courses = this.GetCourse();
            foreach(Courses item in courses)
            {
                if(CourseNo == item.CourseNo)
                {
                    return Convert.ToInt32(item.Credit);
                }
            }
            return 0;
        }
    }
}